module GeneratePhrase ( generatePhrase
                      ) where

import Learn

import qualified Data.Map as M
import qualified Data.Vector as V
import Data.Char
import Data.Maybe
import Data.List

-- Very unsafe function
lkm :: (Num a, Ord a, Ord k) => a -> [k] -> M.Map k a -> k
lkm x (k:ks) m
  | y > x || null ks = k
  | otherwise = lkm (x - y) ks m
  where y = fromJust $ M.lookup k m

generatePhrase :: Int
               -> [Double]
               -> M.Map [String] Int
               -> String
               -> String
generatePhrase n (g:gs) m s
  | null s = generatePhrase n gs m $ unwords k
  | length s < n || ow' == M.empty = generatePhrase n gs m $ s++" "++unwords k'
  | '.' `elem` s = takeWhile (/= '.') s ++ "."
  | otherwise = generatePhrase n gs m (s++" "++ unwords (drop (n-1) k''))
  where
    (swU',swL') = M.partitionWithKey (\k _ -> isUpper $ head $ head k) m
    fwU = M.map ((/(fromIntegral $ M.size swU')) . fromIntegral) swU'
    fwL = M.map ((/(fromIntegral $ M.size swL')) . fromIntegral) swL'
    k = lkm g (M.keys fwU) fwU
    k' = lkm g (M.keys fwL) fwL
    ls = reverse $ take (n-1) $ reverse $ words s
    ow' = M.filterWithKey (\k _ -> ls == init k) m
    ow = M.map ((/(fromIntegral $ M.size ow')) . fromIntegral) ow'
    k'' = lkm g (M.keys ow) ow
