module Learn ( learn
             ) where

import qualified Data.Map as M
import qualified Data.Vector as V
import Data.Maybe
import Data.Char

import NGrams

learn :: Int -> String -> M.Map [String] Int
learn n s = foldl (flip $ M.alter f) M.empty $ nGrams n  $ words $ s
  where
    f x
      | isJust x = fmap (+1) x
      | otherwise = Just 1
