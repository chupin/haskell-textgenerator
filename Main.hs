{-# LANGUAGE BangPatterns #-}

import Learn
import GeneratePhrase

import System.Random
import Control.Monad
import System.Environment
import qualified Data.List.Safe as L
import Data.Maybe
import Data.Char
import System.Exit
import Control.Concurrent
import System.IO
import System.Directory

main = do
  mainid <- newEmptyMVar
  forkIO $ do
    mnid <- takeMVar mainid
    args <- getArgs
    let fm = args L.!! 0 in do
      exist <- maybe (return False) doesFileExist fm
      if isNothing fm || (not exist)
        then do
        killThread mnid
        die "No file to make the database (either no filename was given or the file couldn't be found). Giving up."
        else
        let ng = args L.!! 1 in
         if isJust ng &&
            (not (all isNumber (fromJust ng)) || (read (fromJust ng) <= 1))
         then do
           killThread mnid
           die "The second argument must be an integer superior to two, which is not what you gave me. Giving up."
         else do
           let n = if isNothing ng then 4 else read $ args !! 1
           putStrLn $ "Making " ++
             show n ++ "-grams database from " ++
             fromJust fm ++ "."
           rm <- readFile $ args !! 0
           let !m = learn n rm
           forever $
             do
               gen <- newStdGen
               let g = randomRs (0.0,1.0) gen :: [Double]
               putStrLn $ generatePhrase n g m ""
  do
    hSetBuffering stdin NoBuffering
    id <- myThreadId
    putMVar mainid id
    c <- getChar
    putStrLn ""
    exitSuccess
