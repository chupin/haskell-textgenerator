module NGrams ( nGrams
              ) where

import Data.List

nGrams :: Int -> [a] -> [[a]]
nGrams n = filter ((== n) . length) . map (take n) . tails
