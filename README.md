# Haskell text generator

This program generates a text given a text file as database using N-grams. 
It is entirely written in Haskell.

To compile:

    ghc -O2 Main.hs -o NGrams
    
To use:

    ./NGrams <textfile> [Ngrams size]